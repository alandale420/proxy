FROM registry.gitlab.com/ngat-docker/gentoo-consul

LABEL maintainer="colton@nextgenagritech.com" \
      "com.nextgenagritech.vendor"="NextGen AgriTech" \
      version="1.0.0"

ADD package.accept_keywords/* /etc/portage/package.accept_keywords/
ADD package.use/* /etc/portage/package.use/

# Install app dependancies
# ENV NPM_CONFIG_LOGLEVEL warn
RUN emerge-webrsync \
    && echo " --> Setting nginx USE flags" \
    && echo "NGINX_MODULES_HTTP=\"access addition browser echo geo gunzip gzip gzip_static headers_more javascript limit_conn limit_req lua map metrics proxy push_stream realip referer rewrite secure_link slice ssi sticky stub_status sub upload_progress upstream_check upstream_hash upstream_ip_hash upstream_keepalive upstream_least_conn upstream_zone userid vhost_traffic_status\"" >> /etc/portage/make.conf \
    && echo "NGINX_MODULES_STREAM=\"access geo geoip javascript limit_conn map realip return split_clients ssl_preread upstream_hash upstream_least_conn upstream_zone\"" >> /etc/portage/make.conf \
    && echo " --> Installing Nginx" \
    && emerge -j2 -v geoip nginx \
    && chown nginx.nginx /var/log/consul-template -R \
    && chown nginx.nginx /etc/consul-template.d -R \
    && chown nginx.nginx /etc/nginx/* -R \
    && echo " --> Adding Consul and Nginx to init" \
    && rc-update add nginx default \
    && rc-update add consul-template default \
    && echo " --> Cleaning up" \
    && emerge --depclean --with-bdeps=n \
    && rm -fr /usr/portage/* \
    && rm -fr /tmp/* \
    && rm -fr /var/tmp/* \
    && rm -fr /usr/share/man/* \
    && rm -fr /usr/share/sgml/* \
    && rm -fr /usr/share/doc/* \
    && rm -fr /usr/share/gtk-doc/*

ADD consul.json /etc/consul.d/config.json
ADD consul-template.json /etc/consul-template.d/config.json
ADD templates/*.ctmpl /templates/
ADD consul-template.conf.d /etc/conf.d/consul-template
ADD index.html /var/www/localhost/index.html
ADD nginx.conf /etc/nginx/nginx.conf

VOLUME ["/etc/consul.d", "/etc/consul-template.d", "/var/lib/consul"]

EXPOSE 80/tcp 443/tcp

CMD [ "/sbin/init" ]
